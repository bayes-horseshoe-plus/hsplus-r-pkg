import numpy as np
from hsplus.hib_stats import (E_kappa, m_hib, SURE_hib, DIC_hib)


def numpy_mpmath_wrapper(func):
    def decorated(*args, **kwargs):
        np_args = [np.asarray(a_) for a_ in args]
        np_kwargs = {k_: np.asarray(v_) for k_, v_ in kwargs.items()}

        raw_res = func(*np_args, **np_kwargs)

        if not np.iterable(raw_res):
            res = float(raw_res)
        else:
            res = [float(x_) for x_ in raw_res]

        return res

    return decorated

E_kappa_py = numpy_mpmath_wrapper(E_kappa)
SURE_hib_py = numpy_mpmath_wrapper(SURE_hib)
DIC_hib_py = numpy_mpmath_wrapper(DIC_hib)
m_hib_py = numpy_mpmath_wrapper(m_hib)
