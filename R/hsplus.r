#'
#' hsplus: A package for estimation with the Horseshoe+ prior.
#'
#' A package for computing estimates with the Horseshoe+ and Horseshoe prior.
#' This package is a wrapper for the \code{hsplus} Python package.
#'
#'
#' @docType package
#' @author Brandon T. Willard \email{bwillard@@uchicago.edu}
#' @name hsplus
#' @import PythonInR
#' @references
#' \emph{The Horseshoe+ Estimator of Ultra-Sparse Signals}
#' Anindya Bhadra, Jyotishka Datta, Nicholas G. Polson, Brandon Willard
#' \cr
#' \url{http://arxiv.org/abs/1502.00560}
#' \url{https://bitbucket.org/bayes-horseshoe-plus/hsplus-python-pkg}
#' @name hsplus
NULL

.onLoad <- function(libname, pkgname) {
  if (exists(".hsplusConnected", .GlobalEnv)) 
    return()

	if (!pyIsConnected()) {
	  pyConnect()
	  pyOptions("useNumpy", FALSE)
  }

  #
  # Load the python helper functions.
  #
  pyExecfile(system.file("python", "py_helpers.py", package="hsplus"))

  invisible(assign('.hsplusConnected', TRUE, pos=.GlobalEnv))
}

.onUnload <- function(libname, pkgname) {
  if (!exists(".hsplusConnected", .GlobalEnv)) 
    return()

	if (pyIsConnected()) 
	  pyExit()

  invisible(remove('.hsplusConnected', pos=.GlobalEnv))
}

#'
#' Estimates for a normal regression model with global-local shrinkage
#' via hypergeometric inverted-beta (HIB) priors.
#'
#' The model has following form
#' \deqn{
#'  y \sim N(X \beta, \sigma^2) \\
#'  \beta_i \sim N(0, \sigma^2 \tau^2 \lambda^2_i)
#' }
#'
#' The \eqn{\lambda^2_i} are given \eqn{HIB(a, b, s)} distributions, for which
#' \eqn{a = b = 1/2} and \eqn{s = 0} gives the Horseshoe prior.
#'
#'
#' @param formula an object of class \code{formula} (or one that can be coerced to
#' that class): a symbolic description of the model to be fitted.
#' @param data an optional data frame, list or environment (or object
#' coercible by \code{as.data.frame} to a data frame) containing the
#' variables in the model.  If not found in \code{data}, the
#' variables are taken from \code{environment(formula)}, typically
#' the environment from which \code{lm} is called.
#' @param tau Explicit \eqn{\tau} value(s).  If \code{tau} is a vector, then compute
#' the optimal tau under \code{tau.method}.  If \code{tau} is \code{NULL}, optimize under 
#' \code{tau.method}.
#' @param sigma Observation variance.
#' @param a Hypergeometric inverted-beta model parameter
#' @param b Hypergeometric inverted-beta model parameter
#' @param s Hypergeometric inverted-beta model parameter
#' @param tau.method A string signifying the method used to estimate the 
#' global shrinkage parameter, \eqn{\tau}.  Currently the only option is 
#' \code{"SURE"}, which minimizes Stein's unbiased risk estimate (SURE).
#' @return A fitted model object 
#' @export
#'
hib.lm <- function(formula, data, tau=NULL, sigma=1., a=0.5, b=0.5, s=0., 
                   tau.method="SURE", control=list(...), offset = NULL, ...) {
  call = match.call()

  if (missing(data)) 
    data = environment(formula)

  mf = match.call(expand.dots = FALSE)
  m = match(c("formula"), names(mf), 0L)
  mf = mf[c(1L, m)]
  mf$drop.unused.levels = TRUE
  mf[[1L]] = quote(stats::model.frame)
  mf = eval(mf, parent.frame())

  mt = attr(mf, "terms")
  Y = model.response(mf, "any")
  if (length(dim(Y)) == 1L) {
    nm = rownames(Y)
    dim(Y) = NULL
    if (!is.null(nm)) 
      names(Y) = nm
  }
  X = if (!is.empty.model(mt)) model.matrix(mt, mf, contrasts) else matrix(, NROW(Y), 0L)

  est.method = function(alpha_hat, tau_, X_svd) {
    d = X_svd$d
    kappa_m = E_kappa(alpha_hat * d, sigma, tau_ * d, a=0.5, b=0.5, s=0., n=1)
    kappa_m_m1 = 1 - kappa_m
    alpha_m = kappa_m_m1 * alpha_hat

    V = X_svd$v
    beta_mean = drop(V %*% alpha_m)
    beta_var = drop(V %*% diag(kappa_m_m1 * sigma^2) %*% t(V))

    return(list(beta_mean, beta_var))
  }

  control = do.call(".hs.control", control)

  tau.method.name = tau.method
  tau.method = switch(tau.method,
      "SURE" = .sure.method(tau, control, sigma, a, b, s),
      # TODO: Need more options?
      #"" = NULL, 
      {
        warning("Invalid method argument.  Using SURE.");
        return(.sure.method(tau, control, sigma, a, b, s))
      })

  fit = hib.fit(X, Y, tau.method, est.method, offset)

  fit = c(fit, list(call=call,
                    formula=formula,
                    data=data,
                    tau.method=tau.method.name,
                    control=control))
  class(fit) = c(class(fit), "hib.fit")
  return(fit)
}

hib.fit <- function(X, y, tau.method, est.method, offset=NULL, ...) {

  if (is.null(n <- nrow(X)))
      stop("'x' must be a matrix")
  if (n == 0L)
      stop("0 (non-NA) cases")

  p <- ncol(X)

  if (p == 0L) {
      return(list(coefficients = numeric(), residuals = y, 
                  fitted.values = 0 * y, tau_opt = NA))
  }

  ny <- NCOL(y)

  if (is.matrix(y) && ny == 1)
      y <- drop(y)
  if (!is.null(offset))
      y <- y - offset
  if (NROW(y) != n)
      stop("incompatible dimensions")

  chkDots(...)

  X_svd = svd(X)
  d = X_svd$d
  d_inv = 1/d
  # XXX: should do something better about this.
  d_inv[abs(d) < 1e-5] = 0

  # TODO: use `sweep`? 
  alpha_hat = diag(d_inv) %*% t(X_svd$u) %*% y
  
  tau_opt = tau.method(alpha_hat, d)

  coef.moments = est.method(alpha_hat, tau_opt[['tau']], X_svd)
  coef = coef.moments[[1]]
  coef.var = coef.moments[[2]]

  y_hat = X %*% coef
  residuals = y - y_hat

  fit = list(coefficients=coef,
             coef.var = coef.var,
             residuals=residuals,
             fitted.values=y_hat,
             tau_opt=tau_opt)

  return(fit)
}

.sure.method <- function(tau, control, sigma, a, b, s) {
  if (is.numeric(tau)) {

    .sure.fixed.method <- function(alpha_hat, d) {
      sure_sum = function(t_) sum(SURE_hib(alpha_hat * d, sigma, t_ * d, a, b, s))
      sure_vals = sapply(tau, sure_sum)
      min_idx = which.min(sure_vals)
      tau_opt = tau[min_idx]
      sure_opt = sure_vals[min_idx]
      return(list("tau"=tau_opt,
                  "SURE"=sure_opt))
    }

    return(.sure.fixed.method)
  } else {

    .sure.opt.method <- function(alpha_hat, d) {
      sure_sum = function(t_) sum(SURE_hib(alpha_hat * d, sigma, t_ * d, a, b, s))
      tau_range = control[['tau_range']]
      opt = optimize(sure_sum, tau_range, maximum=FALSE)
      tau_opt = opt$minimum
      return(list("tau"=tau_opt,
                  "SURE"=opt$objective))
    }

    return(.sure.opt.method)
  }
}

.hs.control <- function(tau_range = c(1e-3, 1e2), maxit = 100) {
  if (!is.numeric(tau_range) || any(tau_range < 0))
      stop("values of 'tau' must be > 0")
  if (!is.numeric(maxit) || maxit <= 0)
      stop("maximum number of iterations must be > 0")
  return(list(tau_range = tau_range, maxit = maxit))
}

#'
#' Exact evaluation of the marginal posterior for
#' the hypergeometric inverted-beta (HIB) model.
#' 
#' In its most general form, the HIB is
#' \deqn{
#'     p(y_i, \kappa_i) \propto \kappa_i^{a^\prime - 1} (1-\kappa_i)^{b-1}
#'     \left(1/\tau^2 + (1 - 1/\tau^2) \kappa_i\right)^{-1}
#'     e^{-\kappa_i s^\prime}
#'     \;.
#' }
#' where \eqn{s^\prime = s + y_i^2 / (2\sigma^2)} and
#' \eqn{a^\prime = a + 1/2}.
#' 
#' The marginal posterior is
#' \deqn{
#'     m(y_i; \sigma, \tau) = \frac{1}{\sqrt(2 \pi \sigma^2}
#'     \exp\left(-\frac{y_i^2}{2 \sigma^2}\right)
#'     \frac{\operatorname{B}(a^\prime, b)}{\operatorname{B}(a,b)}
#'     \frac{\Phi_1(b, 1, a^\prime + b, s^\prime, 1 - 1/\tau^2)}{
#'         \Phi_1(b, 1, a + b, s, 1 - 1/\tau^2)}
#' }
#' 
#' The Horseshoe prior has \eqn{a = b = 1/2} and \eqn{s = 0}.
#' 
#' @param y A single observation.
#' @param sigma Observation variance.
#' @param tau Prior variance scale factor.
#' @param a HIB model parameter
#' @param b HIB model parameter
#' @param s HIB model parameter
#' @return Numeric value of \eqn{m(y; \sigma, \tau)}.
#' @export
#'
m_hib <- pyFunction("m_hib_py")

#'
#' Exact evaluation of the marginal posterior for 
#' the Horseshoe prior. 
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term
#' @export
#'
m_hs <- function(y, sigma=1, tau=1) {
  return(m_hib(y, sigma, tau, 0.5, 0.5, 0))
}

#'
#' Compute Stein's unbiased risk estimates (SURE) for a 
#' normal observations model with a hypergeometric 
#' inverted-beta prior (HIB).
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term
#' @param a HIB prior parameter
#' @param b HIB parameter
#' @param s HIB prior parameter
#' @param d Observation scaling parameter
#' @return An array of float values
#' @export
#'
SURE_hib <- pyFunction("SURE_hib_py")

#'
#' Compute Stein's unbiased risk estimates (SURE) 
#' for the Horseshoe prior.
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term
#' @return An array of float values
#' @export
#'
SURE_hs <- function(y, sigma=1., tau=1.) {
  return(SURE_hib(y, sigma, tau, a=0.5, b=0.5, s=0., d=1.))
}


#'
#' Compute the posterior moments under \eqn{\kappa} parameterization for 
#' a normal observations model with hypergeometric inverted-beta prior (HIB).
#'
#' In its most general form, 
#' \deqn{
#'     E(\kappa^n \mid y, \sigma, \tau) &=
#'     \frac{(a^\prime)_n}{(a^\prime + b)_n}
#'     \frac{\Phi_1(b, 1, a^\prime + b + n, s^\prime, 1 - 1/\tau^2)}{
#'         \Phi_1(b, 1, a^\prime + b, s^\prime, 1 - 1/\tau^2)}
#'         \;,
#' }
#' where \eqn{s^\prime = s + y_i^2 / (2\sigma^2)} and
#' \eqn{a^\prime = a + 1/2} for the hypergeometric inverted-beta
#' given by
#' \deqn{
#'     p(y_i, \kappa_i) \propto \kappa_i^{a^\prime - 1} (1-\kappa_i)^{b-1}
#'     \left(1/\tau^2 + (1 - 1/\tau^2) \kappa_i\right)^{-1}
#'     e^{-\kappa_i s^\prime}
#'     \;.
#' }
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term
#' @param a HIB prior parameter
#' @param b HIB prior parameter
#' @param s HIB prior parameter
#' @param n Order of the moment
#' @return An array of float values
#' @export
#'
E_kappa <- pyFunction("E_kappa_py")


#'
#' Compute the posterior moments under \eqn{\kappa} parameterization for 
#' the Horseshoe prior.
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term
#' @param n Order of the moment
#' @return An array of float values
#' @export
#'
E_kappa_hs <- function(y, sigma=1., tau=1., n=0) {
  return(E_kappa(y, sigma, tau, 0.5, 0.5, 0., n))
}


#'
#' Compute DIC estimates for a normal observations model with
#' a hypergeometric inverted-beta prior (HIB).
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term
#' @param a HIB prior parameter
#' @param b HIB prior parameter
#' @param s HIB prior parameter
#' @param d Observation scaling parameter
#' @return An array of float values
#' @export
#'
DIC_hib <- pyFunction("DIC_hib_py")

#'
#' Compute the deviance information criterion (DIC) for the 
#' Horseshoe prior.
#'
#' @param y Observations
#' @param sigma Std. dev. parameter
#' @param tau Global sparsity term 
#' @param d Observation scaling parameter
#' @return An array of float values
#' @export
#'
DIC_hs <- function(y, sigma=1, tau=1, d=1) {
  return(DIC_hib(y, sigma, tau, 0.5, 0.5, 0., d))
}
