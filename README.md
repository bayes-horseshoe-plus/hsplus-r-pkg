---
output:
  md_document:
    variant: markdown_github
---

<!-- this file is inspired by https://raw.githubusercontent.com/hadley/dplyr/master/README.Rmd -->
<!-- README.md is generated from README.Rmd. Please edit that file -->



# `hsplus`

This package provides estimations for the Horseshoe and Horseshoe+ prior.  
It's backed by the Python library [`hsplus`](hsplus),
which contains numeric estimation procedures for the bivariate confluent hypergeometric functions
involved--as well as symbolic [SymPy](sp) implementations.
It uses Python's infinite precision [`mpmath`](mp) library and 
[`PythonInR`](pir) to call Python
from R.

## Installation from R

Currently, remote installation in R (without cloning this repository and building locally) requires 
the [`devtools`][dt] package and something like the following:
```
devtools::install_url("https://bitbucket.org/bayes-horseshoe-plus/hsplus-r-pkg/get/HEAD.zip")
```
then, as usual, load the library with `library(hsplus)`.

To update the Python [`hsplus`](hsplus) library, simply run [`pip`](https://docs.python.org/2.7/installing/)
from the command line:
```
$ pip -U https://bitbucket.org/bayes-horseshoe-plus/hsplus-python-pkg/get/HEAD.tar.gz
```


## Development Setup and Installation


The `pip` install is the same as above, except that you might need to add the
`--force-reinstall` option (and `--no-deps` to avoid reinstalling other packages) 
in order to pick up any non-version changes in the [`hsplus`](hsplus) Python
library.


The following is a quick, simple example of local development for this package in R
(see [here][dev] for more details).
Assuming you've pulled the source from the repository into `~/projects/code/hsplus-r-pkg`:

```R
library(devtools)

options(error=recover)

dev_mode(on=T)

#
#  Assuming you're in the root directory of this repo
#  (and it's named `hsplus-r-pkg`), execute the following
#  to install the package in development mode: 
#
install_local("../hsplus-r-pkg")

#
#  Load all functions and variables into the environment, even
#  those that aren't expected to be exposed in public version of the package.
#
load_all("./", export_all=T)

#
#  Do some work, could be in this file, or another.
#
...

#
#  Check that your changes don't break anything else (and that they
#  work, since one should always write tests for their new code)
#
test("./")

#
#  Run these when you're happy with your changes and want to add
#  any new functions and/or update documentation.
#
document("./")

dev_mode(on=F)

```

[hsplus]:https://bitbucket.org/bayes-horseshoe-plus/hsplus-python-pkg
[pir]:https://cran.r-project.org/web/packages/PythonInR/index.html
[sp]:http://www.sympy.org/en/index.html
[mp]:http://mpmath.org
[dev]:http://adv-r.had.co.nz/Package-development-cycle.html
[dt]:https://github.com/hadley/devtools

